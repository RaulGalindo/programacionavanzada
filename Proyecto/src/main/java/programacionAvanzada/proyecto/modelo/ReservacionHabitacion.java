package programacionAvanzada.proyecto.modelo;

public class ReservacionHabitacion {
	private Habitacion habitacion;
	private Reservacion reservacion;
	
	public Habitacion getHabitacion() {
		return habitacion;
	}
	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}
	public Reservacion getReservacion() {
		return reservacion;
	}
	public void setReservacion(Reservacion reservacion) {
		this.reservacion = reservacion;
	}
	
	
}
