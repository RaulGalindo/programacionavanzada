package programacionAvanzada.proyecto.modelo;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import programacionAvanzada.proyecto.base.BaseHibernateDAO;


public class VendedorDAO extends BaseHibernateDAO{
	public List<Object> getAllVendedores(){
		return findAll(Vendedor.class);
	}
	
	public Vendedor getVendedorById(int idVendedor) {
		Criteria criteria = getSession().createCriteria(Vendedor.class)
				.add(Restrictions.eq("idVendedor", idVendedor));
		return (Vendedor)criteria.uniqueResult();
	}
	
	public List<Object> getAllVendedoresByActivo(){
		Criteria criteria = getSession().createCriteria(Vendedor.class)
				.add(Restrictions.eq("activo", 1));
		return findByCriteria(criteria);
	}
	
	public void saveVendedor(Vendedor a) {
		super.save(a);
	}
	
	public void saveOrUpdateVendedor(Vendedor a) {
		super.saveOrUpdate(a);
	}
	
}
