package programacionAvanzada.proyecto.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


@Entity
@Table(name = "Reservacion", catalog = "proyecto")
public class Reservacion implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int idReservacion;
	private String PrecioTotal;
	private Date FechaInicio;
	private Date FechaFin;
	private Cliente idCliente_FK;
	private Vendedor idVendedor_FK;
	private Habitacion idHabitacion_FK;
	
	public Reservacion() {}
	public Reservacion(int a, String b, Date c, Date d, Cliente e, Vendedor f, Habitacion g) {
		idReservacion=a; PrecioTotal=b; FechaInicio=c; FechaFin=d; idCliente_FK=e; idVendedor_FK=f; idHabitacion_FK=g;
	}
	
	@Id
	@GenericGenerator(name="reservacion_cta" , strategy="increment")
	@GeneratedValue(generator="reservacion_cta")
	@Column(name = "idReservacion", unique = true, nullable = false)
	public int getIdReservacion() {
		return idReservacion;
	}
	
	public void setIdReservacion(int idReservacion) {
		this.idReservacion = idReservacion;
	}
	
	@Column(name = "PrecioTotal", nullable = false, length = 90)
	public String getPrecioTotal() {
		return PrecioTotal;
	}
	public void setPrecioTotal(String PrecioTotal) {
		this.PrecioTotal = PrecioTotal;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FechaInicio", nullable = false)
	public Date getFechaInicio(){
		return FechaInicio;
	}

	public void setFechaInicio(Date FechaInicio){
		this.FechaInicio = FechaInicio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FechaFin", nullable = false)
	public Date getFechaFin(){
		return FechaFin;
	}

	public void setFechaFin(Date FechaFin){
		this.FechaFin = FechaFin;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn (name = "idCliente_FK", nullable = false)
	public Cliente getIdCliente_FK() {
		return idCliente_FK;
	}
	public void setIdCliente_FK(Cliente idCliente_FK) {
		this.idCliente_FK = idCliente_FK; 
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn (name = "idVendedor_FK", nullable = false)
	public Vendedor getIdVendedor_FK() {
		return idVendedor_FK;
	}
	public void setIdVendedor_FK(Vendedor idVendedor_FK) {
		this.idVendedor_FK = idVendedor_FK; 
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn (name = "idHabitacion_FK", nullable = false)
	public Habitacion getIdHabitacion_FK() {
		return idHabitacion_FK;
	}
	public void setIdHabitacion_FK(Habitacion idHabitacion_FK) {
		this.idHabitacion_FK = idHabitacion_FK;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Reservacion [idReservacion=");
		stringBuilder.append(idReservacion);
		stringBuilder.append(", Precio Total =");
		stringBuilder.append(PrecioTotal);
		stringBuilder.append(", Fecha Inicio =");
		stringBuilder.append(FechaInicio);
		stringBuilder.append(", Fecha Fin =");
		stringBuilder.append(FechaFin);
		return stringBuilder.toString();
	}
/*stringBuilder.append(", Ciente =");
		stringBuilder.append(idCliente_FK.toString());
		stringBuilder.append(", Vendedor =");
		stringBuilder.append(idVendedor_FK.toString());*/
}
