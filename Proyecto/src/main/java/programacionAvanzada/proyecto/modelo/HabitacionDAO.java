package programacionAvanzada.proyecto.modelo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import programacionAvanzada.proyecto.base.BaseHibernateDAO;

public class HabitacionDAO extends BaseHibernateDAO{
	public List<Object> getAllHabitaciones(){
		return findAll(Habitacion.class);
	}
	
	public Habitacion getHabitacionById(int idHabitacion) {
		Criteria criteria = getSession().createCriteria(Habitacion.class)
				.add(Restrictions.eq("idHabitacion", idHabitacion));
		return (Habitacion)criteria.uniqueResult();
	}
	
	public void saveHabitacion(Habitacion habitacion) {
		super.save(habitacion);
	}
}
