package programacionAvanzada.proyecto.modelo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "Cliente", catalog = "proyecto")
public class Cliente implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int idCliente;
	private String Nombre;
	private String Apellidos;
	private int Edad;
	private String Email;
	private String Telefono;
	private String Password;
	
	public Cliente() {}
	public Cliente(int id, String Nombre, String Apellidos, int Edad, String Email, String Telefono, String Password){
		this.idCliente=id;
		this.Nombre=Nombre;
		this.Apellidos=Apellidos;
		this.Edad=Edad;
		this.Email=Email;
		this.Telefono=Telefono;
		this.Password=Password;
	}
	
	@Id
	@GeneratedValue(generator = "kaugen")
	@GenericGenerator(name = "kaugen", strategy="increment")
	@Column(name = "idCliente")
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	@Column(name = "Nombre", nullable = false, length = 90)
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}

	@Column(name = "Apellidos", nullable = false, length = 90)
	public String getApellidos(){
		return Apellidos;
	}

	public void setApellidos(String Apellidos){
		this. Apellidos = Apellidos;
	}

	@Column(name = "Edad", nullable = false)
	public int getEdad(){
		return Edad;
	}
	public void setEdad(int Edad){
		this.Edad = Edad;
	}

	@Column(name = "Email", nullable = false, length = 90)
	public String getEmail(){
		return Email;
	}
	public void setEmail(String Email){
		this.Email = Email;
	}

	@Column(name = "Telefono", nullable = false, length = 90)
	public String getTelefono(){
		return Telefono;
	}

	public void setTelefono(String Telefono){
		this.Telefono = Telefono;
	}
	
	@Column(name = "Password", nullable = false, length = 90)
	public String getPassword(){
		return Password;
	}

	public void setPassword(String Password){
		this.Password = Password;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Cliente [idCliente=");
		stringBuilder.append(idCliente);
		stringBuilder.append(", Nombre=");
		stringBuilder.append(Nombre);
		stringBuilder.append(", Apellidos=");
		stringBuilder.append(Apellidos);
		stringBuilder.append(", Edad=");
		stringBuilder.append(Edad);
		stringBuilder.append(", E-mail=");
		stringBuilder.append(Email);
		stringBuilder.append(", Telefono=");
		stringBuilder.append(Telefono);
		stringBuilder.append(", Password=");
		stringBuilder.append(Password);
		stringBuilder.append("]");
		return stringBuilder.toString();
	}

}
