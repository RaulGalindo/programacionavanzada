package programacionAvanzada.proyecto.modelo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import programacionAvanzada.proyecto.controlador.Login;
import programacionAvanzada.proyecto.base.BaseHibernateDAO;

public class ClienteDAO extends BaseHibernateDAO{
	public List<Object> getAllClientes(){
		return findAll(Cliente.class);
	}
	
	public void saveCliente(Cliente cliente) {
		super.save(cliente);
	}
	
	public void saveOrUpdate(Cliente cliente) {
		super.saveOrUpdate(cliente);
	}
	
	public void delete(Cliente cliente) {
		super.delete(cliente);
	}
	
	public Cliente getClienteById(int idCliente) {
		Criteria criteria = getSession().createCriteria(Cliente.class)
				.add(Restrictions.eq("idCliente", idCliente));
		return (Cliente)criteria.uniqueResult();
	}
	
	public Cliente login(Login login) {
		Criteria criteria = getSession().createCriteria(Cliente.class)
				.add(Restrictions.eq("email", login.getMail()) )
				.add(Restrictions.eq("password", login.getPassword()) );
		return (Cliente)criteria.uniqueResult();
	}
}
