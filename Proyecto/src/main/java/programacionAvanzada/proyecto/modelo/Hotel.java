package programacionAvanzada.proyecto.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "Hotel", catalog = "proyecto")
public class Hotel implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int idHotel;
	private String Nombre;
	private String Direccion;
	private String Telefono;
	
	public Hotel() {}
	public Hotel(int a, String b, String c, String d) {idHotel = a; Nombre=b; Direccion=c; Telefono=d;}
	
	@Id
	@GeneratedValue(generator = "kaugen")
	@GenericGenerator(name = "kaugen", strategy="increment")
	@Column(name = "idHotel", unique = true, nullable = false)
	public int getidHotel() {
		return idHotel;
	}
	
	public void setidHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	
	@Column(name = "Nombre", nullable = false, length = 90)
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}

	@Column(name = "Direccion", nullable = false, length = 90)
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String Direccion) {
		this.Direccion = Direccion;
	}

	@Column(name = "Telefono", nullable = false, length = 90)
	public String getTelefono() {
		return Telefono;
	}
	public void setTelefono(String Telefono) {
		this.Telefono = Telefono;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Hotel [id=");
		stringBuilder.append(idHotel);
		stringBuilder.append(", Nombre=");
		stringBuilder.append(Nombre);
		stringBuilder.append(", Direccion=");
		stringBuilder.append(Direccion);
		stringBuilder.append(", Telefono=");
		stringBuilder.append(Telefono);
		stringBuilder.append("]");
		return stringBuilder.toString();
	}

}
