package programacionAvanzada.proyecto.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.criteria.Fetch;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


@Entity
@Table(name = "habitacion", catalog = "proyecto")
public class Habitacion implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int idHabitacion;
	private int Numero;
	private String Espacio;
	private String Camas;
	private String ExtensionTelefono;
	private Date HorarioEntrada;
	private Date HorarioSalida;
	private int NumeroMaximoHuespedes;
	private Hotel hotel_idHotel;
	
	public Habitacion() {}
	
	public Habitacion(int a, int b, String c, String d, String e, Date f, Date g, int h, Hotel i) {
		idHabitacion=a; Numero=b; Espacio=c; Camas=d; ExtensionTelefono=e; HorarioEntrada=f;
		HorarioSalida=g; NumeroMaximoHuespedes=h; hotel_idHotel=i;
	}
	@Id
	@GenericGenerator(name="Habitacion_cta" , strategy="increment")
	@GeneratedValue(generator="Habitacion_cta")
	@Column(name = "idHabitacion", unique = true, nullable = false)
	public int getidHabitacion() {
		return idHabitacion;
	}
	
	public void setidHabitacion(int idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
	
	@Column(name = "Numero", nullable = false)
	public int getNumero() {
		return Numero;
	}
	
	public void setNumero(int Numero) {
		this.Numero = Numero;
	}

	@Column(name="Espacio", nullable=false, length = 90)
	public String getEspacio(){
		return Espacio;
	}

	public void setEspacio(String Espacio){
		this.Espacio = Espacio;
	}

	@Column(name = "Camas", nullable = false, length = 90)
	public String getCamas(){
		return Camas;
	}

	public void setCamas(String Camas){
		this.Camas = Camas;
	}

	@Column(name = "ExtensionTelefono", nullable = false, length = 90)
	public String getExtensionTelefono(){
		return ExtensionTelefono;
	}

	public void setExtensionTelefono(String ExtensionTelefono){
		this.ExtensionTelefono = ExtensionTelefono; 
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "HorarioEntrada", nullable  = false)
	public Date getHorarioEntrada(){
		return HorarioEntrada;
	}

	public void setHorarioEntrada(Date HorarioEntrada){
		this.HorarioEntrada = HorarioEntrada;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "HorarioSalida", nullable  = false)
	public Date getHorarioSalida(){
		return HorarioSalida;
	}

	public void setHorarioSalida(Date HorarioSalida){
		this.HorarioSalida = HorarioSalida;
	}

	@Column(name = "NumeroMaximoHuespedes", nullable = false)
	public int getNumeroMaximoHuespedes(){
		return NumeroMaximoHuespedes;
	}

	public void setNumeroMaximoHuespedes(int NumeroMaximoHuespedes){
		this.NumeroMaximoHuespedes = NumeroMaximoHuespedes;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn (name = "hotel_idHotel", nullable = false)
	public Hotel getHotel_idHotel(){
		return hotel_idHotel;
	}

	public void setHotel_idHotel(Hotel hotel_idHotel){
		this.hotel_idHotel = hotel_idHotel;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Habitacion [id=");
		stringBuilder.append(idHabitacion);
		stringBuilder.append(", Numero=");
		stringBuilder.append(Numero);
		stringBuilder.append(", Espacio=");
		stringBuilder.append(Espacio);
		stringBuilder.append(", Camas=");
		stringBuilder.append(Camas);
		stringBuilder.append(", ExtensionTelefono=");
		stringBuilder.append(ExtensionTelefono);
		stringBuilder.append(", Horario de Entrada =");
		stringBuilder.append(HorarioEntrada);
		stringBuilder.append(", Hoario de Salida =");
		stringBuilder.append(HorarioSalida);
		stringBuilder.append(", Numero Maximo de Huespedes =");
		stringBuilder.append(NumeroMaximoHuespedes);
		stringBuilder.append("Hotel = ");
		stringBuilder.append(hotel_idHotel.toString());
		stringBuilder.append("]");
		return stringBuilder.toString();
	}

}
