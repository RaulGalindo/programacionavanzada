package programacionAvanzada.proyecto.modelo;
import java.util.List;
import programacionAvanzada.proyecto.base.BaseHibernateDAO;

public class HotelDAO extends BaseHibernateDAO{
	public List<Object> getAllHoteles(){
		return findAll(Hotel.class);
	}
	
	public void saveHotel(Hotel hotel) {
		super.save(hotel);
	}
}
