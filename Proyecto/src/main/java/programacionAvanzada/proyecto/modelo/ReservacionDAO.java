package programacionAvanzada.proyecto.modelo;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import programacionAvanzada.proyecto.base.BaseHibernateDAO;

public class ReservacionDAO extends BaseHibernateDAO{
	public List<Object> getAllReservaciones(){
		return findAll(Reservacion.class);
	}
	
	public List<Object> getAllMisReservaciones(Cliente cliente){
		Criteria criteria = getSession().createCriteria(Reservacion.class)
				.add(Restrictions.eq("idCliente_FK", cliente));
		return findByCriteria(criteria);
	}
	
	public Reservacion getReservacionById(int idReservacion) {
		Criteria criteria = getSession().createCriteria(Reservacion.class)
				.add(Restrictions.eq("idReservacion", idReservacion));
		return (Reservacion)criteria.uniqueResult();
	}
	
	public void saveReservacion(Reservacion reservacion) {
		super.save(reservacion);
	}
	
	public void saveOrUpdateReservacion(Reservacion reservacion) {
		super.saveOrUpdate(reservacion);
	}
	
	public void delete(Reservacion reservacion) {
		super.delete(reservacion);
	}
}
