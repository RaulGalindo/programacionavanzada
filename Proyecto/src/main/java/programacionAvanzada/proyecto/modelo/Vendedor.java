package programacionAvanzada.proyecto.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "Vendedor", catalog = "proyecto")
public class Vendedor implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int idVendedor;
	private String Nombre;
	private String Apellidos;
	private String Email;
	private String Telefono;
	private int Activo;

	public Vendedor() {}
	
	public Vendedor(int a, String b, String c, String d, String e, int f) {
		idVendedor = a;
		Nombre = b;
		Apellidos = c;
		Email = d;
		Telefono = e;
		Activo = f;
	}
	@Id
	@GenericGenerator(name="Vendedor_cta" , strategy="increment")
	@GeneratedValue(generator="Vendedor_cta")
	@Column(name = "idVendedor", unique = true, nullable = false)
	public int getIdVendedor() {
		return idVendedor;
	}
	
	public void setIdVendedor(int idVendedor) {
		this.idVendedor = idVendedor;
	}

	@Column(name = "Nombre", nullable = false, length = 90)
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}

	@Column(name = "Apellidos", nullable = false, length = 90)
	public String getApellidos(){
		return Apellidos;
	}

	public void setApellidos(String Apellidos){
		this. Apellidos = Apellidos;
	}

	@Column(name = "Email", nullable = false, length = 90)
	public String getEmail(){
		return Email;
	}
	public void setEmail(String Email){
		this.Email = Email;
	}

	@Column(name = "Telefono", nullable = false, length = 90)
	public String getTelefono(){
		return Telefono;
	}

	public void setTelefono(String Telefono){
		this.Telefono = Telefono;
	}

	@Column(name="Activo", nullable = false)
	public int getActivo(){
		return Activo;
	}

	public void setActivo(int Activo){
		this.Activo = Activo;
	}

	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Vendedor [idVendedor=");
		stringBuilder.append(idVendedor);
		stringBuilder.append(", Nombre=");
		stringBuilder.append(Nombre);
		stringBuilder.append(", Apellidos=");
		stringBuilder.append(Apellidos);
		stringBuilder.append(", E-mail=");
		stringBuilder.append(Email);
		stringBuilder.append(", Telefono=");
		stringBuilder.append(Telefono);
		stringBuilder.append("]");
		return stringBuilder.toString();
	}

}
