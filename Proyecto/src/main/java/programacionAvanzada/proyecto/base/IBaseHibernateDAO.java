package programacionAvanzada.proyecto.base;

import org.hibernate.Session;

public interface IBaseHibernateDAO {
	public Session getSession();
}
