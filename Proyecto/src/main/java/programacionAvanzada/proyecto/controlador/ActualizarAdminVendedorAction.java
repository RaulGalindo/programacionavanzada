package programacionAvanzada.proyecto.controlador;

import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class ActualizarAdminVendedorAction extends BaseAction {
	
	private int misVendedores;
	
	public String actualizarVendedor() {
		VendedorDAO vendedorDAO = new VendedorDAO();
		Vendedor vendedor = vendedorDAO.getVendedorById(misVendedores);
		
		getSession().put("vendedorId", vendedor.getIdVendedor());
		getSession().put("nombreV", vendedor.getNombre());
		getSession().put("apellidosV",vendedor.getApellidos());
		getSession().put("emailV", vendedor.getEmail());
		getSession().put("telefonoV", vendedor.getTelefono());
		return "success";
	}

	public int getMisVendedores() {
		return misVendedores;
	}

	public void setMisVendedores(int misVendedores) {
		this.misVendedores = misVendedores;
	}
	
}
