package programacionAvanzada.proyecto.controlador;

import java.util.Map;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Acción que implementa caracteristicas generales empleadas 
 * por cualquier acción definida en el controlador:
 * <br/>
 * <ul>
 * 	<li>Herencia de {@link ActionSupport}</li>
 * 	<li>Implementación de {@link SessionAware}</li>
 * </ul>
 * @author Juan Jos� Lopez
 *
 */
public class BaseAction extends ActionSupport implements SessionAware{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Expresión regular para verificar la fortaleza de la contraseña
	 */
	public static final String PASSWD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{5,}$";
	
	/**
	 * Expresión regular para validar el correo electrónico
	 */
	public static final String EMAIL_REGEX = "(?i)^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	
	/**
	 * Expresión regular para validar que la cadena sean letras de la A a la Z incluyendo ñ y acentos
	 */
	public static final String SOLO_TEXTO_REGEX = "^[\\pL\\pM\\p{Zs}.-]+$";
	
	/**
	 * Expresión regular para validar que la cadena sean telefonos
	 */
	public static final String SOLO_PHONE_REGEX = "^([0-9]{10})$";
	
	/**
	 * Expresión regular para validar que la cadena sean telefonos
	 */
	public static final String PRECIO_REGEX = "^([1-9]([0-9]){5})$";
	
	/**
	 * Expresión regular para validar que la cadena sean fechas correctas
	 */
	public static final String DATE_REGEX = "^(?:(?:31(\\/)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
	
	// variable para almacenar el objeto session
	private SessionMap<String, Object> session;
	
	public void setSession(Map<String, Object> session) {
		this.session = (SessionMap<String, Object>) session;
	}
	
	protected SessionMap<String, Object> getSession(){
		return this.session;
	}
	
	protected void cerrarrSesion(){
		for (Map.Entry<String, Object> entry : session.entrySet()) {
			session.remove( entry.getKey() );
		}
		session.invalidate();
	}
}
