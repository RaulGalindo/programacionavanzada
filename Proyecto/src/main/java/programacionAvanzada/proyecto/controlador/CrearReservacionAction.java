package programacionAvanzada.proyecto.controlador;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import programacionAvanzada.proyecto.modelo.Reservacion;
import programacionAvanzada.proyecto.modelo.ReservacionDAO;
import programacionAvanzada.proyecto.modelo.Cliente;
import programacionAvanzada.proyecto.modelo.ClienteDAO;
import programacionAvanzada.proyecto.modelo.Habitacion;
import programacionAvanzada.proyecto.modelo.HabitacionDAO;
import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;
import programacionAvanzada.proyecto.controlador.BaseAction;

public class CrearReservacionAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(CrearReservacionAction.class);
	private ReservacionIntermediate insertarReservacion;
	
	public String insertarReservacion() throws ParseException {
		logger.info("insertarReservacion()1: "+insertarReservacion.getIdReservacion());
		logger.info("insertarReservacion()2: "+insertarReservacion.getPrecioTotal());
		logger.info("insertarReservacion()3: "+insertarReservacion.getFechaInicio());
		logger.info("insertarReservacion()4: "+insertarReservacion.getFechaFin());
		int idCliente = (Integer)getSession().get("clienteSesionadoId");
		
		ClienteDAO clienteDAO = new ClienteDAO();
		Cliente cliente = clienteDAO.getClienteById(idCliente);
		
		VendedorDAO vendedorDAO = new VendedorDAO();
		Vendedor vendedor = vendedorDAO.getVendedorById(1);
		
		HabitacionDAO habitacionDAO = new HabitacionDAO();
		Habitacion habitacion = habitacionDAO.getHabitacionById(insertarReservacion.getIdReservacion());
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaInicio = format.parse(insertarReservacion.getFechaInicio());
		Date fechaFin = format.parse(insertarReservacion.getFechaFin());
		
		Reservacion reservacion = new Reservacion(1,insertarReservacion.getPrecioTotal(), 
				fechaInicio, fechaFin, cliente, vendedor, habitacion);
		ReservacionDAO reservacionDAO = new ReservacionDAO();
		reservacionDAO.saveReservacion(reservacion);
		
		return "success";
	}

	public void validate(){
		logger.info("validate()");
		Date diaActual = new Date();
		logger.info("DIa actual: " + diaActual);
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String diaActualStr = format.format(diaActual);
		logger.info("DIa actual String: " + diaActualStr);
		
		if( insertarReservacion.getPrecioTotal() == null || insertarReservacion.getPrecioTotal().isEmpty() ) {
			addFieldError("insertarReservacion.precioTotal", "El precio no debe estar vacio");
		}
		
		else if(insertarReservacion.getPrecioTotal().matches(PRECIO_REGEX) ) {
			addFieldError("insertarReservacion.precioTotal", "El precio no es valido");
		}
		
		else if( insertarReservacion.getFechaInicio() == null || insertarReservacion.getFechaInicio().isEmpty())  {
			addFieldError("insertarReservacion.fechaInicio", "Ingrese la fecha de inicio");
		}
		
		else if( !insertarReservacion.getFechaInicio().matches(DATE_REGEX) ) {
			addFieldError("insertarReservacion.fechaInicio", "El formato de la fecha de inicio es incorrecta, siga este formato dd/mm/yyyy");
		}
		
		else if( diaActualStr.compareTo(insertarReservacion.getFechaInicio()) > 0) {
			addFieldError("insertarReservacion.fechaInicio", "La fecha de inicio de reservacion debe ser mayor a la fecha actual");

		}
		
		else if( insertarReservacion.getFechaFin() == null || insertarReservacion.getFechaFin().isEmpty())  {
			addFieldError("insertarReservacion.fechaFin", "Ingrese la fecha de finalizacion de reservacion");
		}
		
		else if( !insertarReservacion.getFechaFin().matches(DATE_REGEX) ) {
			addFieldError("insertarReservacion.fechaFin", "El formato de la fecha de finalizacion es incorrecta, siga este formato dd/mm/yyyy");
		}
		
		else if( insertarReservacion.getFechaInicio().compareTo(insertarReservacion.getFechaFin()) > 0) {
			addFieldError("insertarReservacion.fechaFin", "La fecha de finalizacion de reservacion debe ser mayor a la fecha de inicio");

		}
	}
	
	
	public ReservacionIntermediate getInsertarReservacion() {
		return insertarReservacion;
	}

	public void setInsertarReservacion(ReservacionIntermediate insertarReservacion) {
		this.insertarReservacion = insertarReservacion;
	}
	
	
}
