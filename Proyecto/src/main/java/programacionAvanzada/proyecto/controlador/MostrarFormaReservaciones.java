package programacionAvanzada.proyecto.controlador;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import programacionAvanzada.proyecto.modelo.Cliente;
import programacionAvanzada.proyecto.modelo.ClienteDAO;
import programacionAvanzada.proyecto.modelo.Habitacion;
import programacionAvanzada.proyecto.modelo.Reservacion;
import programacionAvanzada.proyecto.modelo.ReservacionDAO;
import programacionAvanzada.proyecto.modelo.ReservacionHabitacion;

public class MostrarFormaReservaciones extends BaseAction {
	private static final Logger logger = Logger.getLogger(MostrarFormaReservaciones.class);
	private List<Reservacion> misReservaciones;
	private List<ReservacionHabitacion> reservacionHabitacion;
	
	public String muestraReservaciones() {
		logger.info("muestraReservaciones()");
		
		int idCliente = (Integer) getSession().get("clienteSesionadoId");
		ClienteDAO clienteDAO = new ClienteDAO();
		Cliente cliente = clienteDAO.getClienteById(idCliente);
		logger.info("muestraReservaciones() entero: " + cliente);
		
		ReservacionDAO reservacionDAO = new ReservacionDAO();
		misReservaciones = (List<Reservacion>)(Object)reservacionDAO.getAllMisReservaciones(cliente);
		
		logger.info("muestraReservaciones()2: "+misReservaciones);
		
		reservacionHabitacion = new ArrayList<ReservacionHabitacion>();
		for (Reservacion item: misReservaciones) {
			Habitacion habitacion = item.getIdHabitacion_FK();
			
			ReservacionHabitacion reservacionObj = new ReservacionHabitacion();
			reservacionObj.setHabitacion(habitacion);
			reservacionObj.setReservacion(item);
			reservacionHabitacion.add(reservacionObj);
		}
		
		logger.info("muestraReservaciones()3: "+reservacionHabitacion);
		
		return "success";
	}

	public List<ReservacionHabitacion> getReservacionHabitacion() {
		return reservacionHabitacion;
	}

	public void setReservacionHabitacion(List<ReservacionHabitacion> reservacionHabitacion) {
		this.reservacionHabitacion = reservacionHabitacion;
	}

	public List<Reservacion> getMisReservaciones() {
		return misReservaciones;
	}

	public void setMisReservaciones(List<Reservacion> misReservaciones) {
		this.misReservaciones = misReservaciones;
	}
}
