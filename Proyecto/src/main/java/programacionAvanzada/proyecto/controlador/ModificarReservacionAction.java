package programacionAvanzada.proyecto.controlador;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import programacionAvanzada.proyecto.modelo.Cliente;
import programacionAvanzada.proyecto.modelo.ClienteDAO;
import programacionAvanzada.proyecto.modelo.Habitacion;
import programacionAvanzada.proyecto.modelo.HabitacionDAO;
import programacionAvanzada.proyecto.modelo.Reservacion;
import programacionAvanzada.proyecto.modelo.ReservacionDAO;
import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class ModificarReservacionAction extends BaseAction {
	private ReservacionIntermediate modificarReservacion;
	private static final Logger logger = Logger.getLogger(ModificarReservacionAction.class);

	public String modificarReservacion() throws ParseException {
		logger.info("Obj Reservacion Obtenido: " + modificarReservacion);
		
		int clienteId =(Integer)getSession().get("reservacionIdActualizarCl");
		int vendedorId =(Integer)getSession().get("reservacionIdActualizarVn");
		int habitacionId =(Integer)getSession().get("reservacionIdActualizarHb");
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaInicio = format.parse(modificarReservacion.getFechaInicio());
		Date fechaFin = format.parse(modificarReservacion.getFechaFin());
		
		ClienteDAO clienteDAO = new ClienteDAO();
		Cliente cliente = clienteDAO.getClienteById(clienteId);
		logger.info("Obj Reservacion Obtenido 2: " + cliente);
		
		VendedorDAO vendedorDAO = new VendedorDAO();
		Vendedor vendedor = vendedorDAO.getVendedorById(vendedorId);
		logger.info("Obj Reservacion Obtenido 3: " + vendedor);
		
		HabitacionDAO habitacionDAO = new HabitacionDAO();
		Habitacion habitacion = habitacionDAO.getHabitacionById(habitacionId);
		logger.info("Obj Reservacion Obtenido 4: " + habitacion);
		
		Reservacion miReservacion = new Reservacion(modificarReservacion.getIdReservacion(), modificarReservacion.getPrecioTotal(), 
				fechaInicio, fechaFin, cliente, vendedor, habitacion);
		logger.info("Obj Reservacion Obtenido 5: " + miReservacion);
		
		ReservacionDAO reservacionDAO = new ReservacionDAO();
		reservacionDAO.saveOrUpdateReservacion(miReservacion);
		
		return "success";
	}
	
	public void validate(){
		logger.info("validate()");
		
		Date diaActual = new Date();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String diaActualStr = format.format(diaActual);
		
		if( modificarReservacion.getPrecioTotal() == null || modificarReservacion.getPrecioTotal().isEmpty() ) {
			addFieldError("insertarReservacion.precioTotal", "El precio no debe estar vacio");
		}
		
		else if(modificarReservacion.getPrecioTotal().matches(PRECIO_REGEX) ) {
			addFieldError("insertarReservacion.precioTotal", "El precio no es valido");
		}
		
		else if( modificarReservacion.getFechaInicio() == null || modificarReservacion.getFechaInicio().isEmpty())  {
			addFieldError("insertarReservacion.fechaInicio", "Ingrese la fecha de inicio");
		}
		
		else if( !modificarReservacion.getFechaInicio().matches(DATE_REGEX) ) {
			addFieldError("insertarReservacion.fechaInicio", "El formato de la fecha de inicio es incorrecta, siga este formato dd/mm/yyyy");
		}
		
		else if( diaActualStr.compareTo(modificarReservacion.getFechaInicio()) > 0) {
			addFieldError("insertarReservacion.fechaInicio", "La fecha de inicio de reservacion debe ser mayor a la fecha actual");

		}
		
		else if( modificarReservacion.getFechaFin() == null || modificarReservacion.getFechaFin().isEmpty())  {
			addFieldError("insertarReservacion.fechaFin", "Ingrese la fecha de finalizacion de reservacion");
		}
		
		else if( !modificarReservacion.getFechaFin().matches(DATE_REGEX) ) {
			addFieldError("insertarReservacion.fechaFin", "El formato de la fecha de finalizacion es incorrecta, siga este formato dd/mm/yyyy");
		}
		
		else if( modificarReservacion.getFechaInicio().compareTo(modificarReservacion.getFechaFin()) > 0) {
			addFieldError("insertarReservacion.fechaFin", "La fecha de finalizacion de reservacion debe ser mayor a la fecha de inicio");

		}
	}

	public ReservacionIntermediate getModificarReservacion() {
		return modificarReservacion;
	}

	public void setModificarReservacion(ReservacionIntermediate modificarReservacion) {
		this.modificarReservacion = modificarReservacion;
	}
	
}
