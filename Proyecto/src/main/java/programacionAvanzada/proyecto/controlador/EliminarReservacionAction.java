package programacionAvanzada.proyecto.controlador;

import org.apache.log4j.Logger;

import programacionAvanzada.proyecto.modelo.Reservacion;
import programacionAvanzada.proyecto.modelo.ReservacionDAO;

public class EliminarReservacionAction {

	private int eliminarIdReservacion;
	private static final Logger logger = Logger.getLogger(ActualizarReservacionAction.class);

	public String eliminarIdReservacion() {
		logger.info("Objeto eliminar: "+eliminarIdReservacion);
		
		ReservacionDAO reservacionDAO = new ReservacionDAO();
		Reservacion reservacion = reservacionDAO.getReservacionById(eliminarIdReservacion);
		reservacionDAO.delete(reservacion);
		
		return "success";
	}
	
	public int getEliminarIdReservacion() {
		return eliminarIdReservacion;
	}

	public void setEliminarIdReservacion(int eliminarIdReservacion) {
		this.eliminarIdReservacion = eliminarIdReservacion;
	}
}
