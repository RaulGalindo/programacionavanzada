package programacionAvanzada.proyecto.controlador;

import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class ModificarAdminVendedorAction {

	private Vendedor modificarVendedor;
	
	
	public String modificarVendedor() {
		VendedorDAO vendedorDAO = new VendedorDAO(); 
		Vendedor vendedor = new Vendedor(modificarVendedor.getIdVendedor(), modificarVendedor.getNombre(), modificarVendedor.getApellidos(),
				modificarVendedor.getEmail(), modificarVendedor.getTelefono(), 1);
		vendedorDAO.saveOrUpdateVendedor(vendedor);
		
		return "success";
	}

	public Vendedor getModificarVendedor() {
		return modificarVendedor;
	}

	public void setModificarVendedor(Vendedor modificarVendedor) {
		this.modificarVendedor = modificarVendedor;
	}

}
