package programacionAvanzada.proyecto.controlador;

import org.apache.log4j.Logger;
import programacionAvanzada.proyecto.modelo.Cliente;
import programacionAvanzada.proyecto.modelo.ClienteDAO;
import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class RegistroAction extends BaseAction {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(RegistroAction.class);
	private Cliente clienteRegistro;


	public String clienteRegistro() {
		logger.info("clienteRegistro()"+clienteRegistro.getEmail() + " password: " + clienteRegistro.getPassword());
		
		try {
			logger.info("clienteRegistro() entro al try");
			
			ClienteDAO clienteDAO = new ClienteDAO();
			Cliente cliente = new Cliente(1, clienteRegistro.getNombre(), clienteRegistro.getApellidos(), clienteRegistro.getEdad(),
					clienteRegistro.getEmail(), clienteRegistro.getTelefono(), clienteRegistro.getPassword());
			clienteDAO.saveCliente(cliente);
			logger.info("clienteRegistro() 2:"+cliente);
			
			getSession().put("clienteSesionadoId", cliente.getIdCliente());
			getSession().put("clienteSesionado", cliente.getNombre());
			getSession().put("email", cliente.getEmail());
			
			return "authenticated";
			
		} catch (Exception e) {
			logger.info("clienteRegistro() entro al catch");
			return "not-authenticated";
		}
		
	}
	
	public void validate(){
		logger.info("validate()");
		
		logger.info("Información de usuario a registrar: " + clienteRegistro);
		
		if( clienteRegistro.getEmail() == null || clienteRegistro.getEmail().isEmpty() ) {
			logger.warn("No se recibió correo");
			addFieldError("clienteRegistro.email", "El correo es requerido");
		}
		
		else if( !clienteRegistro.getEmail().matches(EMAIL_REGEX) ) {
			logger.warn("Correo mal formado");
			addFieldError("clienteRegistro.email", "Correo electronico invalido");
		}
		
		else if( clienteRegistro.getNombre() == null || clienteRegistro.getNombre().isEmpty() ) {
			addFieldError("clienteRegistro.nombre", "Ingresa tu nombre por favor");
		}
		
		// Si el nombre tiene caracteres especiales o números
		else if( !clienteRegistro.getNombre().matches(SOLO_TEXTO_REGEX) ) {
			addFieldError("clienteRegistro.nombre", "Tu nombre tiene caracteres invalidos");
		}
		
		else if( clienteRegistro.getApellidos() == null || clienteRegistro.getApellidos().isEmpty() ) {
			addFieldError("clienteRegistro.apellidos", "Ingrese sus apellidos por favor");
		}
		
		else if( !clienteRegistro.getApellidos().matches(SOLO_TEXTO_REGEX) ) {
			addFieldError("clienteRegistro.apellidos", "Apellidos tienen caracteres invalidos");
		}
		
		else if( clienteRegistro.getEdad() < 19 || clienteRegistro.getEdad()>100 ) {
			addFieldError("clienteRegistro.edad", "Ingresa una edad valida por favor, debes ser mayor de edad");
		}
		
		else if( clienteRegistro.getTelefono() == null || clienteRegistro.getTelefono().isEmpty() ) {
			addFieldError("clienteRegistro.telefono", "Ingresa tu telefono por favor");
		}
		
		else if( !clienteRegistro.getTelefono().matches(SOLO_PHONE_REGEX) ) {
			addFieldError("clienteRegistro.telefono", "Tu telefono tiene caracteres invalidos");
		}
		
		else if( clienteRegistro.getPassword() == null || clienteRegistro.getPassword().isEmpty() ) {
			addFieldError("clienteRegistro.password", "Contrasena requerida");
		}
		
	}

	public Cliente getClienteRegistro() {
		return clienteRegistro;
	}

	public void setClienteRegistro(Cliente clienteRegistro) {
		this.clienteRegistro = clienteRegistro;
	}
	
	
}
