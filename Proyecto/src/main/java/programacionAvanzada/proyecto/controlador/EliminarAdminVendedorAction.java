package programacionAvanzada.proyecto.controlador;

import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class EliminarAdminVendedorAction {

	private int eliminarVendedores;

	public String eliminarVendedor() {
		VendedorDAO vendedorDAO = new VendedorDAO();
		Vendedor vendedor = vendedorDAO.getVendedorById(eliminarVendedores);
		vendedor.setActivo(0);
		vendedorDAO.saveOrUpdateVendedor(vendedor);
		
		return "success";
	}
	
	public int getEliminarVendedores() {
		return eliminarVendedores;
	}

	public void setEliminarVendedores(int eliminarVendedores) {
		this.eliminarVendedores = eliminarVendedores;
	}
	
	
}
