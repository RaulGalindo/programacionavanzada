package programacionAvanzada.proyecto.controlador;

import org.apache.log4j.Logger;

import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class RegistraVendedorAction extends BaseAction {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(RegistraVendedorAction.class);
	private Vendedor insertSeller;
	
	public String insertSeller() {
		
		VendedorDAO vendedorDAO = new VendedorDAO();
		Vendedor vendedor = new Vendedor();
		vendedor.setNombre(insertSeller.getNombre());
		vendedor.setApellidos(insertSeller.getApellidos());
		vendedor.setEmail(insertSeller.getEmail());
		vendedor.setTelefono(insertSeller.getTelefono());
		vendedor.setActivo(1);
		vendedor.setIdVendedor(21);
		vendedorDAO.saveVendedor(vendedor);
		logger.info("Vendedor: "+vendedor);
		return "success";
	}
	public Vendedor getInsertSeller() {
		return insertSeller;
	} 
	public void setInsertSeller(Vendedor insertSeller) {
		this.insertSeller = insertSeller;
	}

}
