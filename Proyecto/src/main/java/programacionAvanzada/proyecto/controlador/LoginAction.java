package programacionAvanzada.proyecto.controlador;

import org.apache.log4j.Logger;

import programacionAvanzada.proyecto.modelo.Cliente;
import programacionAvanzada.proyecto.modelo.ClienteDAO;

public class LoginAction extends BaseAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(LoginAction.class);
	private Login clienteLogin;

	public String clienteLogin() {
		logger.info("loginUsuario()"+clienteLogin.getMail() + " password: " + clienteLogin.getPassword());
		
		try {
			logger.info("loginUsuario() entro al try");
			
			ClienteDAO clienteDAO = new ClienteDAO();
			Cliente cliente = clienteDAO.login(clienteLogin);
			
			logger.info("loginUsuario() 2:"+cliente);
			if (cliente == null) {
				return "not-found";
			}
			
			getSession().put("clienteSesionadoId", cliente.getIdCliente());
			getSession().put("clienteSesionado", cliente.getNombre());
			getSession().put("email", cliente.getEmail());
			
			if ((clienteLogin.getMail().equals("admin@fiestainn.com")) && (clienteLogin.getPassword().equals("admin"))) {
				logger.info("Entro a ser admin");
				return "authenticated-admin";
			}
			
			return "authenticated";
		} catch (Exception e) {
			logger.info("loginUsuario() entro al catch");
			return "not-found";
		}
		
	}
	
	public String logoutUsuario() {
		logger.info("Cerrando sesión de usuario");
		cerrarrSesion();
		return "success";
	}
	
	public String logoutAdministrador() {
		logger.info("Cerrando sesión de administrador");
		cerrarrSesion();
		return "success";
	}
	
	
	public Login getClienteLogin() {
		return clienteLogin;
	}
	public void setClienteLogin(Login clienteLogin) {
		this.clienteLogin = clienteLogin;
	}
	
}
