package programacionAvanzada.proyecto.controlador;


public class ReservacionIntermediate {
	private int idReservacion;
	private String PrecioTotal;
	private String FechaInicio;
	private String FechaFin;
	
	public int getIdReservacion() {
		return idReservacion;
	}
	public void setIdReservacion(int idReservacion) {
		this.idReservacion = idReservacion;
	}
	public String getPrecioTotal() {
		return PrecioTotal;
	}
	public void setPrecioTotal(String precioTotal) {
		PrecioTotal = precioTotal;
	}
	public String getFechaInicio() {
		return FechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return FechaFin;
	}
	public void setFechaFin(String fechaFin) {
		FechaFin = fechaFin;
	}
}
