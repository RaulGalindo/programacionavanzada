package programacionAvanzada.proyecto.controlador;

import java.util.List;

import org.apache.log4j.Logger;
import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class MostrarFormaAdminVendedores {
	private static final Logger logger = Logger.getLogger(MostrarFormaAdminVendedores.class);
	private List<Vendedor> misVendedores;
	
	public String muestraVendedores() {
		VendedorDAO vendedorDAO = new VendedorDAO();
		misVendedores = (List<Vendedor>)(Object)vendedorDAO.getAllVendedoresByActivo();
		
		logger.info("Mis vendedores: " + misVendedores);
		return "success";
	}

	public List<Vendedor> getMisVendedores() {
		return misVendedores;
	}

	public void setMisVendedores(List<Vendedor> misVendedores) {
		this.misVendedores = misVendedores;
	}
	
}
