package programacionAvanzada.proyecto.controlador;

import java.util.List;
import org.apache.log4j.Logger;
import programacionAvanzada.proyecto.modelo.Habitacion;
import programacionAvanzada.proyecto.modelo.HabitacionDAO;
import programacionAvanzada.proyecto.modelo.Hotel;
import programacionAvanzada.proyecto.modelo.HotelDAO;
import programacionAvanzada.proyecto.modelo.Cliente;


public class MostrarFormaHabitaciones {
	private static final Logger logger = Logger.getLogger(MostrarFormaHabitaciones.class);
	private List<Habitacion> misHabitaciones;
	
	public String showHabitaciones() {
		logger.info("showHabitaciones()");
		
		return "success";
	}
	
	public String muestraHabitaciones() {
		logger.info("muestraHabitaciones()");
		HabitacionDAO habitacionDAO = new HabitacionDAO();
		misHabitaciones = (List<Habitacion>)(Object)habitacionDAO.getAllHabitaciones();
		
		return "success";
	}

	public List<Habitacion> getMisHabitaciones() {
		return misHabitaciones;
	}

	public void setMisHabitaciones(List<Habitacion> misHabitaciones) {
		this.misHabitaciones = misHabitaciones;
	}

	
}
