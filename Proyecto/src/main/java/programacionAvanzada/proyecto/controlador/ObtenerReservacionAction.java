package programacionAvanzada.proyecto.controlador;

import org.apache.log4j.Logger;

public class ObtenerReservacionAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(ObtenerReservacionAction.class);
	private int optionMiHabitacion;
	
	public String obtenerIdHabitacion() {	
		logger.info("Variable ID Habitacion: "+optionMiHabitacion);
		getSession().put("optionMiHabitacion", optionMiHabitacion);
		return "success";
	}

	public int getOptionMiHabitacion() {
		return optionMiHabitacion;
	}

	public void setOptionMiHabitacion(int optionMiHabitacion) {
		this.optionMiHabitacion = optionMiHabitacion;
	}
}
