package programacionAvanzada.proyecto.controlador;
import java.util.List;
import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class MostrarFormaAdminEliminarVendedor {
	private List<Vendedor> eliminarVendedores;
	
	public String eliminaVendedor() {
		VendedorDAO vendedorDAO = new VendedorDAO();
		eliminarVendedores = (List<Vendedor>)(Object)vendedorDAO.getAllVendedoresByActivo();
		
		return "success";
	}

	public List<Vendedor> getEliminarVendedores() {
		return eliminarVendedores;
	}

	public void setEliminarVendedores(List<Vendedor> eliminarVendedores) {
		this.eliminarVendedores = eliminarVendedores;
	}

}
