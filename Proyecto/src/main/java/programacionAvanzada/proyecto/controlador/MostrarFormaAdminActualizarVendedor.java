package programacionAvanzada.proyecto.controlador;

import java.util.List;

import programacionAvanzada.proyecto.modelo.Vendedor;
import programacionAvanzada.proyecto.modelo.VendedorDAO;

public class MostrarFormaAdminActualizarVendedor {
	
	private List<Vendedor> misVendedores;

	
	public String modificaVendedor() {
		
		VendedorDAO vendedorDAO = new VendedorDAO();
		misVendedores = (List<Vendedor>)(Object)vendedorDAO.getAllVendedoresByActivo();
		
		return "success";
	}
	
	
	public List<Vendedor> getMisVendedores() {
		return misVendedores;
	}
	public void setMisVendedores(List<Vendedor> misVendedores) {
		this.misVendedores = misVendedores;
	}
}
