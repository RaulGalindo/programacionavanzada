package programacionAvanzada.proyecto.controlador;

import org.apache.log4j.Logger;
import programacionAvanzada.proyecto.modelo.Reservacion;
import programacionAvanzada.proyecto.modelo.ReservacionDAO;

public class ActualizarReservacionAction extends BaseAction {
	private int actualizarIdReservacion;
	private Reservacion reservacionIdActualiza;
	private static final Logger logger = Logger.getLogger(ActualizarReservacionAction.class);
	
	public String actualizarIdReservacion() {
		logger.info("Actualizar Id: " + actualizarIdReservacion);
		
		ReservacionDAO reservacionDAO = new ReservacionDAO();
		reservacionIdActualiza = reservacionDAO.getReservacionById(actualizarIdReservacion);
		logger.info("Actualizar Id 2: " + reservacionIdActualiza);
		
		getSession().put("reservacionIdActualizar", actualizarIdReservacion);
		getSession().put("reservacionIdActualizarPt", reservacionIdActualiza.getPrecioTotal());
		getSession().put("reservacionIdActualizarFi", reservacionIdActualiza.getFechaInicio());
		getSession().put("reservacionIdActualizarFf", reservacionIdActualiza.getFechaFin());
		getSession().put("reservacionIdActualizarCl", reservacionIdActualiza.getIdCliente_FK().getIdCliente());
		getSession().put("reservacionIdActualizarVn", reservacionIdActualiza.getIdVendedor_FK().getIdVendedor());
		getSession().put("reservacionIdActualizarHb", reservacionIdActualiza.getIdHabitacion_FK().getidHabitacion());
		
		logger.info("Actualizar Id 3: " + reservacionIdActualiza.getIdCliente_FK().getIdCliente());
		logger.info("Actualizar id 4: " + reservacionIdActualiza.getIdVendedor_FK().getIdVendedor());
		logger.info("Actualizar Id 5: " + reservacionIdActualiza.getIdHabitacion_FK().getidHabitacion());
		
		return "success";
	}

	public Reservacion getReservacionIdActualiza() {
		return reservacionIdActualiza;
	}

	public void setReservacionIdActualiza(Reservacion reservacionIdActualiza) {
		this.reservacionIdActualiza = reservacionIdActualiza;
	}

	public int getActualizarIdReservacion() {
		return actualizarIdReservacion;
	}

	public void setActualizarIdReservacion(int actualizarIdReservacion) {
		this.actualizarIdReservacion = actualizarIdReservacion;
	}
	
	
}
