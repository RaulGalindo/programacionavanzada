<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>

<link href="${pageContext.request.contextPath}/css/general-style.css" rel="stylesheet"/>
<link href="css/login-styles.css" rel="stylesheet">
<link href="css/bootstrap-3.3.7.min.css" rel="stylesheet">
<script src="js/bootstrap-3.3.7.min.js"></script>

<title>Menu Cliente</title>

</head>
<body>
	<div id="contenidoBox"><br>
	<h2>Bienvenido(a) ${sessionScope.clienteSesionado} a tu Menu</h2>
	<p><a href='<s:url action="muestraHabitaciones" />'>Ver Habitaciones Disponibles</a></p>
	<p><a href='<s:url action="muestraReservaciones" />'>Ver Mis Reservaciones</a></p>
	<p><a href='<s:url action="logoutUsuario" />'>Cerrar Sesion</a></p>
	</div>
</body>
</html>

