<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<link href="${pageContext.request.contextPath}/css/admin-style.css" rel="stylesheet"/>

<title>Menu Administrador</title>
</head>
<body><br>
<div id="contenidoBox"><br>
	<h2>Buen dia Administrador.</h2><br>
	<p><a href='<s:url action="muestraVendedores" />'>Consultar Vendedores</a></p><br>
	<p><a href='<s:url action="insertaVendedor" />'>Agregar Vendedor</a></p><br>
	<p><a href='<s:url action="modificaVendedor" />'>Actualizar Vendedor</a></p><br>
	<p><a href='<s:url action="eliminaVendedor" />'>Eliminar Vendedor</a></p><br>
	<p><a href='<s:url action="logoutAdministrador" />'>Cerrar Sesion</a></p><br>
</div></body>
</html>

