<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
	<link href="css/login-styles.css" rel="stylesheet">
	<link href="css/bootstrap-3.3.7.min.css" rel="stylesheet">
	<script src="js/bootstrap-3.3.7.min.js"></script>
</head>
<title> Login Reservaciones </title>
<body>
	<div class="container">
		<div class="card card-container">
			<h4>Es un gusto que te registres con nosotros!</h4>
			<img id="profile-img" class="profile-img-card"
				src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
			<p id="profile-name" class="profile-name-card"></p>			
			<form action="<s:url action="clienteRegistro" namespace="/"/>" method="POST" class="form-signin">
				<span id="reauth-email" class="reauth-email"></span>
				 <s:fielderror/>
				 <s:textfield name="clienteRegistro.email" class="form-control" placeholder="Ingresa tu email por favor"></s:textfield> 
				 <s:textfield name="clienteRegistro.nombre" class="form-control" placeholder="Ingresa tu nombre por favor"></s:textfield> 
				 <s:textfield name="clienteRegistro.apellidos" class="form-control" placeholder="Ingresa tus apellidos por favor"></s:textfield> 
				 <s:textfield name="clienteRegistro.edad" class="form-control" placeholder="Ingresa tu edad por favor"></s:textfield> 
				 <s:textfield name="clienteRegistro.telefono" class="form-control" placeholder="Ingresa tu telefono por favor"></s:textfield> 
				 <s:password name="clienteRegistro.password" class="form-control" placeholder="Ingresa tu contraseña por favor"></s:password>
				<button class="btn btn-lg btn-primary btn-block btn-signin"
					type="submit">Registrarse</button>
			</form><!-- /form -->
		</div><!-- /card-container -->
	</div><!-- /container -->
</body>
</html>
