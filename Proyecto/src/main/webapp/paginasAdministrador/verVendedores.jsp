<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/admin-style.css" rel="stylesheet"/>
<title>Vendedores Actuales</title>
</head>
<body id="body4">
<div id="contenidoBox2"><br>
	<h2>Vendedores Actuales</h2><br>
		<table border=1px;> <tr><td> Nombre </td>
				<td> Apellidos </td>
				<td> Telefono </td>
				<td> Correo electronico </td> </tr>
				
		<c:forEach items="${misVendedores}" var="miVendedor">
			
			<tr><td> ${miVendedor.nombre} </td>
				<td> ${miVendedor.apellidos} </td>
				<td> ${miVendedor.telefono} </td>
				<td> ${miVendedor.email} </td> </tr>
		</c:forEach>
	</table>
	<br>
	<p><a href='<s:url action="muestraMenuAdmin" />'>Regresar a Menu</a></p><br><br><br><br><br><br><br>
	<br><br><br><br><br><br><br><br><br><br><br></div>
</body>
</html>
