<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/admin-style.css" rel="stylesheet"/>
<title>Eliminar Vendedor</title>
</head>
<body>
<div id="contenidoBox"><br><h2><b>Por favor, seleccione al vendedor que desea eliminar</b></h2><br>
	<table><tr><td></td><td>
	<s:form action="eliminarVendedor" namespace="/admin" method="POST">	
		<select name="eliminarVendedores">
			<c:forEach items="${eliminarVendedores}" var="miVendedor">
				<option value="${miVendedor.idVendedor}"> ${miVendedor.nombre} ${miVendedor.apellidos}</option>
			</c:forEach>
		</select><br></td></tr><tr><td></td><td>
		<p id="parrafo1"><s:submit value="Eliminar vendedor" /></p>
	</s:form><br>
	<p><a href='<s:url action="muestraMenuAdmin" />'>Regresar a Menu</a></p><br><br><br><br><br><br><br></td></tr></table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>
</body>
</html>
