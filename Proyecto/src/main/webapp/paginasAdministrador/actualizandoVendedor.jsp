<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/admin-style.css" rel="stylesheet"/>
<title>Actualizar Reservacion</title>
</head>
<body>
<div id="contenidoBox"><br><h2><b>Por favor, modifique los datos del vendedor</b></h2><br>
	<s:form action="modificarVendedor" namespace="/admin" method="POST">
		<s:fielderror/>
		<s:textfield name="modificarVendedor.idVendedor" value="%{#session.vendedorId}" label="ID Vendedor" readonly="true" ></s:textfield>
		<s:textfield name="modificarVendedor.nombre" value="%{#session.nombreV}" label="Nombre"></s:textfield>
		<s:textfield name="modificarVendedor.apellidos" value="%{#session.apellidosV}" label="Apellidos"></s:textfield>
		<s:textfield name="modificarVendedor.email" value="%{#session.emailV}" label="Correo"></s:textfield>
		<s:textfield name="modificarVendedor.telefono" value="%{#session.telefonoV}" label="Telefono"></s:textfield>
		<s:submit value="Actualizar Vendedor" />
	</s:form><br>
	<p><a href='<s:url action="muestraMenuAdmin" />'>Regresar a Menu</a></p><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>
</body>
</html>
