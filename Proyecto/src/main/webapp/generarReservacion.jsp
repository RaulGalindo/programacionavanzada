<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/general-style.css" rel="stylesheet"/>
<title>Generar Reservacion</title>
</head>
<body>
	<div id="contenidoBox"><br><h3><b> Generar reservacion</b></h3><h5>Por favor, ingrese los datos de la reservacion</h5><br>
	<s:form action="insertarReservacion" namespace="/clientes" method="POST">
		
		<s:fielderror/>
		<s:textfield name="insertarReservacion.idReservacion" value="%{#session.optionMiHabitacion}" label="ID Habitacion" readonly="true"> </s:textfield>
		<s:textfield name="insertarReservacion.precioTotal" value="1500" label="Precio Total" readonly="true"></s:textfield>
		<s:textfield name="insertarReservacion.fechaInicio" label="Fecha Inicio"></s:textfield>
		<s:textfield name="insertarReservacion.fechaFin" label="Fecha Fin"></s:textfield>
		<s:submit value="Reservar" />
		
	</s:form><br><br><br><br><br><br><br><br>
	</div>
</body>
</html>
