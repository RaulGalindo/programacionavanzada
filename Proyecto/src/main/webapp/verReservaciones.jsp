<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/general-style.css" rel="stylesheet"/>
<title>Mis Reservaciones</title>
</head>
<body id="body2"><div id="contenidoBox2">
	<h2>Ver mis reservaciones!</h2>
	<p>Mis Reservaciones</p><br>
	<c:if test="${reservacionHabitacion.isEmpty() == false}">
	<p>
		<table border=1px;> <tr><td><b> idReservacion </b></td>
				<td><b> Numero de Habitacion</b> </td>
				<td><b> Caracteristicas </b></td>
				<td> <b>Fecha Inicio </b></td>
				<td> <b>Fecha Fin </b></td>
				<td><b> Precio </b></td>
				<td><b> Horario de Entrada </b></td>
				<td><b> Horario de Salida </b></td>
				<td><b> Modificar</b> </td>
				<td><b> Eliminar </b></td> </tr>
				
		<c:forEach items="${reservacionHabitacion}" var="miReservacion">
			
			<tr><td> ${miReservacion.reservacion.idReservacion} </td>
				<td> ${miReservacion.habitacion.numero} </td>
				<td> ${miReservacion.habitacion.camas} </td>
				<td> ${miReservacion.reservacion.fechaInicio} </td>
				<td> ${miReservacion.reservacion.fechaFin} </td>
				<td> $ ${miReservacion.reservacion.precioTotal} </td>
				<td> ${miReservacion.habitacion.horarioEntrada} </td>
				<td> ${miReservacion.habitacion.horarioSalida} </td>
				<td><s:form action="actualizarIdReservacion" namespace="/clientes" method="POST"> <button name="actualizarIdReservacion"
				 type="submit" value="${miReservacion.reservacion.idReservacion}">Actualizar</button> </s:form> </td>
				<td><s:form action="eliminarIdReservacion" namespace="/clientes" method="POST"> <button name="eliminarIdReservacion"
				 type="submit" value="${miReservacion.reservacion.idReservacion}">Eliminar</button> </s:form> </td> </tr>
		</c:forEach>
	</table></p>
	<br>
	</c:if>
	<c:if test="${reservacionHabitacion.isEmpty() == true}">
		<h3> Aun no tienes reservaciones! Si deseas hacer una ingresa a Ver Habitaciones Disponibles por favor. </h3><br>
	</c:if>
	<p><a href='<s:url action="muestraMenu" />'>Regresar a Menu</a></p><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>
</body>
</html>
