<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/general-style.css" rel="stylesheet"/>
<title>Habitacionesl</title>
</head>
<body id="body2">
	<div id="contenidoBox80">
	<br><h3>Hola ${sessionScope.clienteSesionado} estas son las habitaciones disponibles.</h3>
	<s:form action="obtenerIdHabitacion" namespace="/clientes" method="POST">	
		<select name="optionMiHabitacion" style="width:220px; height:30px">
			<c:forEach items="${misHabitaciones}" var="miHabitacion">
				<option value="${miHabitacion.idHabitacion}"> ${miHabitacion.camas} </option>
			</c:forEach>
		</select><br>
		<p id="parrafo1"><input type="submit" class="boton" value="Reservar Habitacion" /></p>
	</s:form>	
	</div>
	<div id="contenidoBox20">
	<p><a href='<s:url action="muestraReservaciones" />'>Ver Mis Reservaciones</a></p>
	<p><a href='<s:url action="muestraMenu" />'>Regresar a Menu</a></p>
	</div>
</body>
</html>

