<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/admin-style.css" rel="stylesheet"/>
<title>Agregar Vendedor</title>
</head>
<body>
<div id="contenidoBox"><br>
	<h2>Por favor, ingrese los datos para agregar al vendedor</h2><br>
	<s:form action="insertSeller" namespace="/admin" method="POST">
		<span id="reauth-email" class="reauth-email"></span>
		<s:fielderror/>
		<s:textfield name="insertSeller.nombre" label="Nombre" > </s:textfield>
		<s:textfield name="insertSeller.apellidos" label="Apellidos"></s:textfield>
		<s:textfield name="insertSeller.telefono" label="Telefono"></s:textfield>
		<s:textfield name="insertSeller.email" label="Correo"></s:textfield>
		<s:submit value="Registrar Vendedor"/>
	</s:form><br>
	<p><a href='<s:url action="muestraMenuAdmin" />'>Regresar a Menu</a></p><br><br><br><br><br><br><br><br><br><br><br><br><br>
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>
</body>
</html>
