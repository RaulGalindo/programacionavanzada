<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/general-style.css" rel="stylesheet"/>
<title>Actualizar Reservacion</title>
</head>
<body>
<div id="contenidoBox"><br><h3><b> Actualizar reservacion</b></h3><h5>Por favor, modifique los datos de la reservacion</h5><br>
	<s:form action="modificarReservacion" namespace="/clientes" method="POST">
		<s:fielderror/>
		<s:textfield name="modificarReservacion.idReservacion" value="%{#session.reservacionIdActualizar}" label="ID Reservacion" readonly="true" ></s:textfield>
		<s:textfield name="modificarReservacion.precioTotal" value="%{#session.reservacionIdActualizarPt}" label="Precio Total" readonly="true"></s:textfield>
		<s:textfield name="modificarReservacion.fechaInicio" value="%{#session.reservacionIdActualizarFi}" label="Fecha Inicio"></s:textfield>
		<s:textfield name="modificarReservacion.fechaFin" value="%{#session.reservacionIdActualizarFf}" label="Fecha Fin"></s:textfield>
		<s:submit value="Actualizar Reservacion" />
	</s:form><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>
</body>
</html>
